<META HTTP-EQUIV="Content-Type" CONTENT="text/html; charset=tis-620">

<style>
* {
  -webkit-box-sizing: border-box;
  -moz-box-sizing: border-box;
  box-sizing: border-box;
}

body {
  padding: 20px 15%;
}
form header {
  margin: 0 0 20px 0; 
}
form header div {
  font-size: 90%;
  color: #999;
}
form header h2 {
  margin: 0 0 5px 0;
}
form > div {
  clear: both;
  overflow: hidden;
  padding: 1px;
  margin: 0 0 10px 0;
}
form > div > fieldset > div > div {
  margin: 0 0 5px 0;
}
form > div > label,
legend {
	width: 25%;
  float: left;
  padding-right: 10px;
}
form > div > div,
form > div > fieldset > div {
  width: 75%;
  float: right;
}
form > div > fieldset label {
	font-size: 90%;
}
fieldset {
	border: 0;
  padding: 0;
}

input[type=text],
input[type=email],
input[type=url],
input[type=password],
textarea {
	width: 100%;
  border-top: 1px solid #ccc;
  border-left: 1px solid #ccc;
  border-right: 1px solid #eee;
  border-bottom: 1px solid #eee;
}
input[type=text],
input[type=email],
input[type=url],
input[type=password] {
  width: 50%;
}
input[type=text]:focus,
input[type=email]:focus,
input[type=url]:focus,
input[type=password]:focus,
textarea:focus {
  outline: 0;
  border-color: #4697e4;
}

@media (max-width: 600px) {
  form > div {
    margin: 0 0 15px 0; 
  }
  form > div > label,
  legend {
	  width: 100%;
    float: none;
    margin: 0 0 5px 0;
  }
  form > div > div,
  form > div > fieldset > div {
    width: 100%;
    float: none;
  }
  input[type=text],
  input[type=email],
  input[type=url],
  input[type=password],
  textarea,
  select {
    width: 100%; 
  }
}
@media (min-width: 1200px) {
  form > div > label,
	legend {
  	text-align: right;
  }
}
</style>

<form action="kutimetable.php" method="post">

  <header>
    <h2>KUtimetable</h2>
    <div>v.2.0.1 beta</div>
  </header>
  
  <div>
    <label class="desc" id="title1" for="Field1">id</label>
    <div>
      <input id="Field1" name="id" type="text" class="field text fn" value="" size="8" tabindex="1">
    </div>
  </div>
  
  <div>
    <label class="desc" id="title106" for="Field106">
    	Year
    </label>
    <div>
    <select id="Field106" name="year" class="field select medium" tabindex="2"> 
		<option value="57">57</option>
        <option value="56">56</option>
		<option value="55">55</option>
		<option value="54">54</option>
		<option value="53">53</option>
		<option value="52">52</option>
		<option value="51">51</option>
    </select>
    </div>
  </div>
  
   <div>
    <label class="desc" id="title106" for="Field106">
    	Semester
    </label>
    <div>
    <select id="Field107" name="sem" class="field select medium" tabindex="3"> 
        <option value="0">summer</option>
		<option value="1">first</option>
		<option value="2">second</option>
		<option value="3">third</option>  
		<option value="4">summer(intern for vet/forest)</option>  
		<option value="5">summer(2)</option>  
    </select>
    </div>
  </div> 

  <div>
    <fieldset>
      <legend id="title6" class="desc">
        save to google drive or not (can't be used now)
      </legend>
      <div>
      <div>
      	<input id="Field6" name="Field6" type="checkbox" value="save" tabindex="8" disabled="disabled"/>
      	<label class="choice" for="Field6">save</label>
      </div>
    </fieldset>
  </div>  
  
<div>
    <fieldset>
      <legend id="title7" class="desc">
        debug mode
      </legend>
      <div>
      <div>
      	<input id="Field7" name="debug" type="checkbox" value="true" tabindex="9"/>
      	<label   class="choice" for="Field6">enabled</label>
      </div>
    </fieldset>
  </div>    
  
 <div>
    <fieldset>
      <legend id="title8" class="desc">
        save as xls
      </legend>
      <div>
      <div>
      	<input id="Field8" name="isXls" type="checkbox" value="true" tabindex="10"/>
      	<label   class="choice" for="Field8">enabled</label>
      </div>
    </fieldset>
  </div>     
  
<div>
    <fieldset>
      <legend id="title9" class="desc">
        if you have 5days-subject, use this
      </legend>
      <div>
      <div>
      	<input id="Field9" name="isToggle1" type="checkbox" value="true" tabindex="11"/>
      	<label   class="choice" for="Field9">enabled</label>
      </div>
    </fieldset>
  </div>       
  
  <div>
		<div>
		<input type="submit" class="btn btn-primary" id="saveForm" name="saveForm" value="Submit" tabindex="4" style="width:100%" />
    </div>
	</div>
  
</form>

<br> by thanawatnew lokiju.net
